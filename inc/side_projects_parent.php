<?php
//echo "test";
include get_template_directory_uri()."/assets/php/discography.php";
      // echo $post->ID;
        

       $args = array(
           'post_type'      => 'page',
           'posts_per_page' => -1,
           'post_parent'    => $post->ID
         //  'meta_key' => 'release_date',
          // 'order'          => 'DESC',
          // 'orderby'        => 'release_date'
        );
       
       
       $parent = new WP_Query( $args );
       
       if ( $parent->have_posts() ) : ?>
       <?php 
        
       $catbtns = array();
           echo "<span class='discography-btn active-btn' id='all'>Show All</span>";
       while ( $parent->have_posts() ) : $parent->the_post();
       $cat = get_field( "category" );
       
       $catbtns[] = $cat;
       
       //$result = array_unique($input);
       
       endwhile;
       $uniquecatbtns = array_unique($catbtns);
       sort($uniquecatbtns);
       foreach ($uniquecatbtns as $catbtn) {
           $fcat = str_replace(' ', '_', $catbtn);
           echo "<span class='discography-btn' id='$fcat'>$catbtn</span>";
       }
       ?>
       <div class="row">
       
       
       
       
       
       
           <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
           <?php $image = get_field( "image" );
           $category = get_field("category");
           $release = get_field("release_date");
           $size = 'full'; // (thumbnail, medium, large, full or custom size)
           $cat = get_field("category");
           $fscat = str_replace(' ', '_', $cat);
          
           ?>
           <div class="col-sm-3 discography-panel <?php echo $fscat ?>">
               <a href="<?php the_permalink(); ?>">
               <div id="parent-<?php the_ID(); ?>" class="parent-page">
                   <?php  if( !empty( $image ) ): ?>
           <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
       <?php endif; ?>
                   <h4><title="<?php the_title(); ?>"><?php the_title(); ?></h4>
       
                   <p><?php echo $release ?></p>
                   <p><?php echo $category ?></p>
       
               </div></a></div>
       
           <?php endwhile; ?>
         
       <?php endif; wp_reset_postdata(); 
       #        include('path/to/file.php'); 
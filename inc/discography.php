<div class="Center">
		<p></p><h1><?php the_title(); ?> </h1><p></p>
	<div class="container"><div class="track-single-container"><div class="row"><div class="col-sm-6 track-left-pane">
    <?php $image = get_field( "image" );
    $category = get_field("category");
    $release = get_field("release_date");
    $label  = get_field("record-label");
    $size = 'full'; // (thumbnail, medium, large, full or custom size)
    $cat = get_field("category");
    $add_to_any = get_field("add_to_any");
    $listen_and_buy = get_field("listen_and_buy")?>
   
            <?php  if( !empty( $image ) ): ?>
    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
<?php endif; ?>
<?php  if( !empty( $listen_and_buy ) ): ?>
<a href="<?php echo $listen_and_buy?>" class="listen-and-buy-btn" target="_blank">Listen &amp; Buy</a>
<?php endif; ?>
            <p><b>Release Date: </b><?php echo $release;?></p>
            <p><b>Release Date: </b><?php echo $label;?></p>
            <?php  if( !empty( $add_to_any ) ): ?>
          
            <div class="add-to-any-block">
            <?php echo $add_to_any;?>
            </div>
            <?php endif; ?>
    </div><div class="col-sm-6">
	<?php
	if( have_posts() ){
		
		while( have_posts() ){
			
			the_post();
			the_content();
		}
	}
	?>
	<?php if( is_page('discography')) {
        include get_template_directory_uri()."/assets/php/discography.php";
       
        

$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'meta_key' => 'category',
    'order'          => 'ASC',
    'orderby'        => 'category'
 );


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>
<?php 
$catbtns = array();
    echo "<span class='discography-btn active-btn' id='all'>Show All</span>";
while ( $parent->have_posts() ) : $parent->the_post();
$cat = get_field( "category" );
$catbtns[] = $cat;

//$result = array_unique($input);

endwhile;
$uniquecatbtns = array_unique($catbtns);

foreach ($uniquecatbtns as $catbtn) {
   $fcat = str_replace(' ', '_', $catbtn);
    echo "<span class='discography-btn' id='$fcat'>$catbtn</span>";
}
?>
<div class="row">






    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
    <?php $image = get_field( "image" );
    $category = get_field("category");
   
    $label = get_field("record-label");
    $size = 'full'; // (thumbnail, medium, large, full or custom size)
    $cat = get_field("category");
    $fscat = str_replace(' ', '_', $cat);
    echo "Test";
    ?>
    <div class="col-sm-3 discography-panel <?php echo $fscat ?>">
        <a href="<?php the_permalink(); ?>">
        <div id="parent-<?php the_ID(); ?>" class="parent-page">
            <?php  if( !empty( $image ) ): ?>
    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
<?php endif; ?>
            <h4><title="<?php the_title(); ?>"><?php the_title(); ?></h4>

            <p><?php echo $release ?></p>
            <p><?php echo $category ?></p>

        </div></a></div>

    <?php endwhile; ?>
  
<?php endif; wp_reset_postdata(); 
#        include('path/to/file.php'); 
    } ?>
  
  
    </div>
    </div></div></div></div>
 <?php 

// END FOR SINGLE TRACK AND START MAIN PAGE.PHP

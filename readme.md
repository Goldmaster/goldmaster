<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://assets.gitlab-static.net/uploads/-/system/project/avatar/13644432/Goldmaster_Profile_Picture.png?width=64">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Goldmaster</h3>

  <p align="center">
    An awesome Wordpress theme template for any content creators website!
    <br />
    <a href="https://gitlab.com/Goldmaster/goldmaster#installation"><strong>How to install »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/Goldmaster/goldmaster/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=">Report Bug</a>
    ·
    <a href="https://gitlab.com/Goldmaster/goldmaster/-/issues/new?issuable_template=incident&issue%5Bissue_type%5D=incident">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements-and-useful-sites">Acknowledgements and useful sites</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

[Goldmaster Demo](https://goldmaster.site)

There are many great wordpress themes available on the web, however, I didn't find one that really suited my needs so I created this wordpress theme. My main insperation was [Mobys website](https://moby.com) which is built in wordpress.

Here's why:
* I could not find an original or functional wordpress theme, that looked great and allowed the user to navigate the website content with ease. 
* I wanted to stick with my old html coded website, and bring it more updated with better mobile support as well as better features such being able to edit posts with laoding up dreamweaver all the time.
* I wanted to create a theme, so I can say this is my website that I designed :smile:


### Built With

* [Hamburgers](https://jonsuh.com/hamburgers/) (Not litrally)
* Help from Daniel
* Bit of reading from [W3 Schools](https://www.w3schools.com/)



<!-- GETTING STARTED -->
## Getting Started

Please read below, so you know how to get started with this theme.

### Prerequisites

Make sure you have wordpress installed and working.


### Installation

1. Download using the blue download button near top right of this page.

2. Extract theme folder to 
   ```sh
   wp-content\themes
   ```
3. Make sure that the files such as `style.css` are in the folder `goldmaster`.

4. The final direcotry strcture should be,
	```sh
	wordpress website files\wp-content\themes\Goldmaster
   ```



<!-- USAGE EXAMPLES -->
## Usage

This theme can be used for products, or a portfolio with easy sorting of catagories.

**Social Media & Music Link Icons**

There are a varity of social media icons available under `assets\images`. The music Icons are in the `music` folder. While the social media icons are in the `socials` folder.

To add the icons into your site, 1st login to your wordpress site. Then select `screen options` go to `appearence` then select `menus`. Then select a menu to edit (either `Music` or `Socials`). Next select `screen options` top right. Then enable the checkbox for `link Target` and `css classes`. Then to make icons appear add the code,

`<i class="icon-MUSIC OR SOCIAL MEDIA PLATFORM NAME HERE"></i>MUSIC OR SOCIAL MEDIA PLATFORM NAME HERE` into the `Navigation Label`.

Just be sure to replace the `CAPITALISED TEXT` with your the platform you want to add.

for exsample if someone wants to add [Vero](https://vero.co/) to their website, the code would be,

`<i class="icon-Vero"></i>Vero`

Please note that not all social media links may be visable, if you have [an adblocker](https://ublockorigin.com) running or [tracking protection](https://support.mozilla.org/en-gb/kb/content-blocking) enabled.

**Share buttons**

To add sharing links for pages, You can use [AddToAny](https://www.addtoany.com/) to add social media share buttons for nearly every social media and messaging platform out there.

Simply go to `pages` then edit (or use `create` to create a new page). Then scroll down to bottom right and under `page attributes`, select `Distography` or whatever your parent page is.

Finally add the code,

`<!-- AddToAny BEGIN --> <div class="a2a_kit a2a_kit_size_32 a2a_default_style"> <a class="a2a_dd" href="https://www.addtoany.com/share"></a> <a class="a2a_button_copy_link"></a> <a class="a2a_button_diaspora"></a> <a class="a2a_button_facebook"></a> <a class="a2a_button_mastodon"></a> <a class="a2a_button_twitter"></a> <a class="a2a_button_reddit"></a> <a class="a2a_button_threema"></a> <a class="a2a_button_facebook_messenger"></a> <a class="a2a_button_telegram"></a> <a class="a2a_button_printfriendly"></a> </div> <script> var a2a_config = a2a_config || {}; a2a_config.onclick = 1; a2a_config.num_services = 10; </script> <script async src="https://static.addtoany.com/menu/page.js"> </script> <!-- AddToAny END -->`

Into the `add to any field` text box (or wherever you want the buttons to appear).

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/Goldmaster/goldmaster/-/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

Please make sure you have followed the **[WordPress Coding Standards](https://codex.wordpress.org/WordPress_Coding_Standards)** ([xkcd](https://xkcd.com/927/)). For any naming convertions you use, make sure they are **all** lowercase with **no numbers** and **contain a Hyphen (-)** (otherwise known as a dash) **between each word** with **no spaces**.

For example if I want to name something as 

(`Listen & Buy`) then title the naming convenention as (`listen-and-buy`)

To contribute, please follow the steps below (if git does work)

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

Or you could cheat and simply use dreamweaver to do the commands for you. 😎



<!-- LICENSE -->
## License

Distributed under the GNU License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Jack Gold (Designer) - [@Goldmaster](https://twitter.com/goldmaster)

Daniel Vaughan (Code geek) - [@plexaweb](https://twitter.com/plexaweb) - [contact](https://www.plexaweb.co.uk/contact)

Project Link: [gitlab.com/goldmaster/goldmaster](https://gitlab.com/goldmaster/goldmaster)



<!-- ACKNOWLEDGEMENTS AND USEFUL SITES -->
## Acknowledgements and useful sites
* [Wordpress Theme Detector](https://www.wpthemedetector.com/)
* [Builtwith](https://builtwith.com/)
* [Best .htaccess Snippets to Improve WordPress Security](https://www.wpexplorer.com/htaccess-wordpress-security/)
* [Mozilla Observatory](https://observatory.mozilla.org/)



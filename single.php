<?php get_header(); ?>



<?php if ( is_front_page() && is_home() ):
    ?>
    
    <?php endif ?>
    <div class="container">
    <div class="row">
    <div id="ttr_main" class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
<div id="ttr_content" class="row">
<?php if (have_posts()) : while (have_posts()) : the_post(); 
if ( is_front_page() && is_home() ):
    ?>

<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
<h2 class="list-title"><?php the_title(); ?></h2>
<h4>Posted on <?php the_time('jS F Y') ?></h4>
<p><?php the_excerpt(__('(more...)', 'goldmaster'));?></p>
</div>
     <?php else: ?>
        <div class="postb">
<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
    
<h1><?php the_title(); ?></h1>
<?php
if ( function_exists('yoast_breadcrumb') ) {
  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
}
?>
<?php
if( function_exists("is_shop") ) {
    // call it or do something else
}
else {
    echo "<h4>Posted on <?php the_time('jS F Y') ?></h4>";
}
?>

<p><?php (the_content(__('(more...)', 'goldmaster')));?></p>
<?php
if( function_exists("is_shop") ) {
    // call it or do something else
}
else {?>

<?php
}
?>
</div>
     </div>
<?php endif ?>
<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.', 'goldmaster'); ?></p>
<?php endif; ?>
</div>
</div>
<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
<?php

function goldmaster_theme_support(){
	// Adds dynamic title tag support
add_theme_support('title-tag');
}

add_action('after_setup_theme','goldmaster_theme_support');

add_theme_support( 'automatic-feed-links' );

function register_navi_menus() {
    register_nav_menus(
			array(
				'menu-1' => __( 'Primary', 'goldmaster' ),
				'menu-music' => __( 'Footer Music', 'goldmaster' ),
                'menu-links' => __( 'Footer links', 'goldmaster' )
			)
		);}
        add_action( 'init', 'register_navi_menus' );


 /*enqueue scripts and CSS */
function goldmaster_register_scripts(){
    $version = wp_get_theme()->get( 'Version' );
   
wp_enqueue_style( 'goldmaster-ext-imported-css', get_template_directory_uri() . '/assets/css/external.css', false, '1.0', 'all' ); 
	wp_enqueue_style('goldmaster-style', get_template_directory_uri()."/style.css", array(), $version, 'all');
    wp_enqueue_script('goldmaster-script', get_template_directory_uri()."/assets/js/goldmaster.js", array(), $version, true);

}
add_action( 'wp_enqueue_scripts', 'goldmaster_register_scripts'); 


/* end of enqueue */
function wpdocs_excerpt_more( $more ) {
    if ( ! is_single() ) {
        $more = sprintf( ' <a class="read-more" href="%1$s">%2$s</a>',
            get_permalink( get_the_ID() ),
            __( 'Read More', 'goldmaster' )
        );
    }
 
    return $more;
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

// Define path and URL to the ACF plugin.
define( 'MY_ACF_PATH', get_stylesheet_directory() . '/inc/advanced-custom-fields/' );
define( 'MY_ACF_URL', get_stylesheet_directory_uri() . '/inc/advanced-custom-fields/' );

// Include the ACF plugin.
include_once( MY_ACF_PATH . 'acf.php' );

// Customize the url setting to fix incorrect asset URLs.
add_filter('acf/settings/url', 'my_acf_settings_url');
function my_acf_settings_url( $url ) {
    return MY_ACF_URL;
}




// (Optional) Hide the ACF admin menu item.
add_filter('acf/settings/show_admin', 'my_acf_settings_show_admin');
function my_acf_settings_show_admin( $show_admin ) {
    return false;
}

function plexaweb_theme_setup() {

    // Add <title> tag support
    add_theme_support( 'title-tag' );  

    // Add custom-logo support
    add_theme_support( 'custom-logo' );

}
add_action( 'after_setup_theme', 'plexaweb_theme_setup');

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_63853b02d11a1',
        'title' => 'Goldmaster Parent',
        'fields' => array(
            array(
                'key' => 'field_63853b033b23c',
                'label' => 'Show child pages in tabbed layout',
                'name' => 'show_child_pages_in_tabbed_layout',
                'aria-label' => '',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'No' => 'No',
                    'Yes' => 'Yes',
                ),
                'default_value' => false,
                'return_format' => 'value',
                'multiple' => 0,
                'allow_null' => 0,
                'ui' => 0,
                'ajax' => 0,
                'placeholder' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'page_type',
                    'operator' => '!=',
                    'value' => 'child',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0,
    ));
    
    acf_add_local_field_group(array(
        'key' => 'group_638537c8804bc',
        'title' => 'Goldmaster Tabbed',
        'fields' => array(
            array(
                'key' => 'field_638537c8e2d50',
                'label' => 'Category',
                'name' => 'category',
                'aria-label' => '',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'maxlength' => '',
                'placeholder' => 'Tab category, text here must match other items to be within the same tab.',
                'prepend' => '',
                'append' => '',
            ),
            array(
                'key' => 'field_6385382fe2d51',
                'label' => 'Image',
                'name' => 'image',
                'aria-label' => '',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'return_format' => 'array',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '',
                'preview_size' => 'medium',
            ),
            array(
                'key' => 'field_6020797084821',
                'label' => 'Add to Any',
                'name' => 'add_to_any',
                'aria-label' => '',
                'type' => 'text',
                'instructions' => 'Place code for add to any here',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_6385386ee2d52',
                'label' => 'Link Text',
                'name' => 'link_text',
                'aria-label' => '',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'maxlength' => '',
                'placeholder' => 'Link Label',
                'prepend' => '',
                'append' => '',
            ),
            array(
                'key' => 'field_6385387de2d53',
                'label' => 'Link',
                'name' => 'link',
                'aria-label' => '',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'maxlength' => '',
                'placeholder' => 'https://www.example.com',
                'prepend' => '',
                'append' => '',
            ),
            array(
                'key' => 'field_6385388ce2d54',
                'label' => 'Release Date',
                'name' => 'release_date',
                'aria-label' => '',
                'type' => 'date_picker',
                'instructions' => 'Release date (Useful for a Discography)',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'display_format' => 'd/m/Y',
                'return_format' => 'd/m/Y',
                'first_day' => 1,
            ),
            array(
                'key' => 'field_638538c7e2d55',
                'label' => 'Record Label',
                'name' => 'record_label',
                'aria-label' => '',
                'type' => 'text',
                'instructions' => 'Record Label (Useful for a Discography)',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'maxlength' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'page_type',
                    'operator' => '==',
                    'value' => 'child',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0,
    ));
    
    endif;	
?>
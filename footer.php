<?php 
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 */
  ?>

	

	<!--Start of center social media icons-->

	<div class="row">
		<div class="social-column">
			<h3>Music</h3>
            <?php wp_nav_menu( array( 'theme_location' => 'menu-music' ) ); ?>

		
		</div>


		<div class="social-column">
			<h3>Socials</h3>
            <?php wp_nav_menu( array( 'theme_location' => 'menu-links' ) ); ?>

		
		</div>
	</div>
	<!--End of social media icons-->


	<p>&nbsp;</p>
	<!--Start of Base -->


	<div class="flex-container">
		<a>Made with 💧, 🥵 & 😢</a>
	</div>


	<p style="text-align: Center"><a href="#Top">Back to top</a>
	</p>
	<span style="text-align: Center"></span>

   
    <?php wp_footer(); ?>
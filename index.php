<?php
get_header();
?>
<h1>News From The Den</h1>
<div class="row">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php
if ( is_home() ):
    // Default homepage
?>
<style>
.news-blocks {
    background-color: #fff;
    color: #000;
    padding: 20px;
    border-radius: 2px;
    margin-bottom: 20px;
}


.anything {
    background: #fff;
}

.news-blocks img {
    max-width: 100% !important;
    width: 100% !important;
    height: auto !important;
}

.nb-img-readmore {
    margin-top: 10px;
    display: inline-block;

}
</style>

<div class="col-sm-6">
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="news-blocks" >
<h2><?php the_title(); ?></h2>
<h4>Posted on <?php the_time('jS F Y') ?></h4>
<?php
if ( has_post_thumbnail() ) {
    the_post_thumbnail();
    echo '<p><a class="read-more nb-img-readmore" href="'. get_permalink($post->ID) . '">Read More</a></p>';
  } else {
    echo "<p>".the_excerpt(__('(more...)', 'goldmaster'))."</p>";
  } 
?>

</div>
</div>
</div>
<?php
endif;
endwhile;
?>
<?php wp_link_pages(array('next_or_number'=>'next', 'previouspagelink' => ' &laquo; ', 'nextpagelink'=>' &raquo;'));?>
</div>
<?php
endif;
get_footer();

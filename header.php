<?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"><?php

	        wp_head();

	        ?>
	
	<link href="Favicon.ico" rel="shortcut icon" title="Favicon">

	<link href="<?php echo get_stylesheet_directory_uri().'/assets/css/hamburgers.min.css'; ?>" rel="stylesheet">
	<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
</head>

<body <?php body_class( $class ); ?>>
<?php wp_body_open(); ?>
	<!--Home button icon-->


	<div class="home-menu-home mobile-logo">
		<a class="" href="https://goldmaster.site"></a>
	</div>
    <a class="logo">
    <?php if ( has_custom_logo() ) : ?>
        <div class="site-branding">
        <?php the_custom_logo(); ?>
        </div>
    <?php else: ?>
        <div class="site-branding">
           <!-- <h1 class="hotwp-site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <p class="hotwp-site-description"><?php bloginfo( 'description' ); ?></p>-->
        </div>
    <?php endif; ?>
    </a>


	<div class="mobile-nav">
		<!--Hamburger button-->
		<button class="hamburger hamburger--spring" type="button"><span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
	</div>


	


	<div class="cmenu-home-menu-container">
		<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_class' => 'menu-home-menu-containerc' ) ); ?>
	</div>
    <div class="clear">
		&nbsp;
	</div>

	<div class="center">